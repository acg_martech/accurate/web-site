$(".carousel").carousel({
  interval: 5000,
});
//=================Cube====================//
$(document).ready(function () {
  var step = 2;
  $(".cube").addClass("step1");
  $(".step1-img1").addClass("slideRight").css("display", "block");
  $(".step1-img2").addClass("slideLeft").css("display", "block");
  setInterval(function () {
    if (step == 5) {
      step = 1;
      $(".cube").removeClass("step4");
    }
    if (step == 1) {
      setTimeout(function () {
        $(".step1-img1").addClass("slideRight").css("display", "block");
        $(".step1-img2").addClass("slideLeft").css("display", "block");
      }, 1000);
      setTimeout(function () {
        $(".step2-img1").removeClass("slideDown").css("display", "none");
        $(".step2-img2").removeClass("fadeIn").css("display", "none");
      }, 1000);
    }
    if (step == 2) {
      setTimeout(function () {
        $(".step2-img2").addClass("fadeIn").css("display", "block");
      }, 1000);
      setTimeout(function () {
        $(".step2-img1").addClass("slideDown").css("display", "block");
      }, 1000);
      $(".step3-img1").removeClass("fadeIn").css("display", "none");
      $(".step3-img2").removeClass("slideUp").css("display", "none");
    }
    if (step == 3) {
      setTimeout(function () {
        $(".step3-img1").addClass("fadeIn").css("display", "block");
      }, 1000);
      setTimeout(function () {
        $(".step3-img2").addClass("slideUp").css("display", "block");
      }, 1000);
      $(".step4-img1").removeClass("slideLeft").css("display", "none");
      $(".step4-img2").removeClass("fadeIn").css("display", "none");
      $(".step4-img3").removeClass("slideRight").css("display", "none");
      $(".step4-img4").removeClass("slideUp").css("display", "none");
    }
    if (step == 4) {
      setTimeout(function () {
        $(".step4-img1").addClass("slideLeft").css("display", "block");
        $(".step4-img2").addClass("fadeIn").css("display", "block");
        $(".step4-img3").addClass("slideRight").css("display", "block");
      }, 1000);
      setTimeout(function () {
        $(".step4-img4").addClass("slideUp").css("display", "block");
      }, 1000);
      $(".step1-img1").removeClass("slideRight").css("display", "none");
      $(".step1-img2").removeClass("slideLeft").css("display", "none");
    }
    $(".cube")
      .addClass("step" + step)
      .removeClass("step" + (step - 1));
    step++;
  }, 4000);
});

//=======================Shop===========================//
$(window).on("load", function () {
  $(document).ready(function () {
    $(".divs div").each(function (e) {
      if (e != 0) $(this).hide();
    });

    $("#next").click(function () {
      if ($(".divs div:visible").next().length != 0)
        $(".divs div:visible").next().show().prev().hide();
      else {
        $(".divs div:visible").hide();
        $(".divs div:first").show();
      }
      return false;
    });

    $("#prev").click(function () {
      if ($(".divs div:visible").prev().length != 0)
        $(".divs div:visible").prev().show().next().hide();
      else {
        $(".divs div:visible").hide();
        $(".divs div:last").show();
      }
      return false;
    });
  });
});
//=================Selección====================//
var clickTag0 = "https://www.ariadnacommunicationsgroup.com/";
var clickTag1 = "https://co.linkedin.com/company/ariadna";
var clickTag2 = "https://www.facebook.com/AriadnaCommunicationsGroup/";
var clickTag3 = "https://www.behance.net/ariadnacg";

var clickArea1 = document.getElementById("click-area1");
clickArea1.onclick = function () {
  window.open(clickTag0, "blank");
};
var clickArea2 = document.getElementById("click-area2");
clickArea2.onclick = function () {
  window.open(clickTag1, "blank");
};
var clickArea3 = document.getElementById("click-area3");
clickArea3.onclick = function () {
  window.open(clickTag2, "blank");
};
var clickArea4 = document.getElementById("click-area4");
clickArea4.onclick = function () {
  window.open(clickTag3, "blank");
};

$(document).ready(function () {
  $("#opc1").click(function () {
    $("#click-area1").addClass("showbtn");
    $("#click-area1").removeClass("hidden");
    $("#click-area2, #click-area3, #click-area4").removeClass("showbtn");
    $("#click-area2, #click-area3, #click-area4").addClass("hidden");
    $("#imgProductos").addClass("producto1");
    $("#imgProductos").removeClass("producto2 producto3 producto4");
    $("#opc1").addClass("active");
    $("#opc2, #opc3, #opc4").removeClass("active");
  });
  $("#opc2").click(function () {
    $("#click-area2").addClass("showbtn");
    $("#click-area2").removeClass("hidden");
    $("#click-area1, #click-area3, #click-area4").removeClass("showbtn");
    $("#click-area1, #click-area3, #click-area4").addClass("hidden");
    $("#imgProductos").addClass("producto2");
    $("#imgProductos").removeClass("producto1 producto3 producto4");
    $("#opc2").addClass("active");
    $("#opc1, #opc3, #opc4").removeClass("active");
  });
  $("#opc3").click(function () {
    $("#click-area3").addClass("showbtn");
    $("#click-area3").removeClass("hidden");
    $("#click-area1, #click-area2, #click-area4").removeClass("showbtn");
    $("#click-area1, #click-area2, #click-area4").addClass("hidden");
    $("#imgProductos").addClass("producto3");
    $("#imgProductos").removeClass("producto1 producto2 producto4");
    $("#opc3").addClass("active");
    $("#opc1, #opc2, #opc4").removeClass("active");
  });
  $("#opc4").click(function () {
    $("#click-area4").addClass("showbtn");
    $("#click-area4").removeClass("hidden");
    $("#click-area1, #click-area2, #click-area3").removeClass(
      "<showbtn></showbtn>"
    );
    $("#click-area1, #click-area2, #click-area3").addClass("hidden");
    $("#imgProductos").addClass("producto4");
    $("#imgProductos").removeClass("producto1 producto2 producto3");
    $("#opc4").addClass("active");
    $("#opc1, #opc2, #opc3").removeClass("active");
  });
});

//======================Expandible=========================//
var EPL = new eplBuilder().create(eplFormats.expandibleRollover);

function eplOnload() {
  var replegado = document.getElementById("replegado");
  var expandido = document.getElementById("expandido");
  var realizarComplete = false;
  var seCompleto = false;

  function expande() {
    replegado.className = "ocultaReplegado";
    expandido.className = "muestraExpandido";
    realizarComplete = true;
    AnuncioCompleto();
  }

  function repliega() {
    expandido.className = "ocultoExpandido";
    replegado.className = "muestraReplegado";
    realizarComplete = false;
  }

  function AnuncioCompleto() {
    setTimeout(function () {
      if (seCompleto == false) {
        if (realizarComplete == true) {
          seCompleto = true;
          EPL.adComplete();
        }
      }
    }, 8000);
  }

  replegado.onmouseover = function () {
    expande();
  };
  expandido.onmouseout = function () {
    repliega();
  };

  if (replegado.offsetWidth < 800) {
    setInterval(function () {
      if (replegado.className == "muestraReplegado") {
        expande();
      } else {
        repliega();
      }
    }, 5000);
  }
}

//======================FlipBanner===========================//
function Transformar() {
  var banners = document.querySelector(".flip").children;
  var time = 0;
  var i = 0;
  for (i; i < banners.length; i++) {
    var el = banners[i].querySelector(".flip-card-inner");
    setTimeOut(el, time);
    time = time + 2000;
    console.log(el, time);
  }
  console.log(banners, time, i);
}

function setTimeOut(el, time) {
  setTimeout(function () {
    el.style.transform = "rotateY(180deg)";
    setTimeout(function () {
      el.style.transform = "";
    }, 1000);
  }, time);
}

setInterval(Transformar(), 8000);

//=======================  ====================//
function redirigir(e) {
  switch (e.id) {
    case "carrusel1":
      window.location.href = "seleccion.html";
      break;
    case "carrusel2":
      window.location.href = "expandible.html";
      break;
    case "carrusel3":
      window.location.href = "flip.html";
      break;
    case "carrusel4":
      window.location.href = "cubo.html";
      break;
    case "carrusel5":
      window.location.href = "shop.html";
      break;
    default:
      break;
  }
}
